/** @file main.h
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 11:47
 *
 *  @brief 
 *
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include "TP.h"
#include "lib/samIO.h"
#include <time.h>

#endif // __MAIN_H__
