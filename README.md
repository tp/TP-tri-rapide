# TP-tri-rapide

## Compilation

pour compiler c'est simple !!

```bash
gcc -Wall -Wextra -pedantic lib/samIO.c lib/samTabDyn.c TP.c tris.c main.c -o TP_tris
```

## Utilisation

Pour lancer :

```bash
./TP_tris
```

## Temps d'execution

![Temps d'execution pour 100 000 elements](images/Graph_Tris_100000.png)

Pour 1 000 000 d'elements les algorithmes de tri lent on etait negligé car beaucoup trop lent comparer aux tris rapides.

![Temps d'execution pour 1 000 000 elements](images/Graph_Tris_rapide.png)
