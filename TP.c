/** @file TP.c
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 11:47
 *
 *
 */

#include "TP.h"

TabInt1D randTab(int n, int Inf, int Sup)
{
    TabInt1D tab;
    int int_ligne;

    iniTabInt1D(&tab, n, 0);

    for (int_ligne = 0; int_ligne < tab.nbCases; int_ligne++)
    {
        tab.tableau[int_ligne] = (random() % (Sup - Inf)) + Inf;
    }

    return tab;
}

void copieTab(TabInt1D *tab1, TabInt1D *tab2)
{
    iniTabInt1D(tab2, tab1->nbCases, 0);
    int int_case;
    for (int_case = 0; int_case < tab2->nbCases; int_case++)
    {
        tab2->tableau[int_case] = tab1->tableau[int_case];
    }
}

void executeTri(int numTri, TabInt1D *tab)
{
    switch (numTri)
    {
    case 0:
        triInsertion(tab);
        break;
    case 1:
        triBulles(tab);
        break;
    case 2:
        triSelection(tab);
        break;
    case 3:
        triRapide(tab);
        break;
    case 4:
        triFusion(tab);
        break;
    case 5:
        triShell(tab);
        break;
    default:
        fprintf(stderr, "le Tris selectioner n'existe pas ou n'est pas implementer.");
        break;
    }
}

void execTestTri(int taille, float tabTemps[6])
{
    TabInt1D tabRef = randTab(taille, 0, taille);
    TabInt1D tab;
    clock_t t1, t2;
    int int_i;
    //afficheTabInt1D(tabRef);
    for (int_i = 0; int_i < 6; int_i++)
    {
        copieTab(&tabRef, &tab);
        t1 = clock();
        executeTri(int_i, &tab);
        t2 = clock();
        tabTemps[int_i] = (float)(t2 - t1) / CLOCKS_PER_SEC;
        //afficheTabInt1D(tab);
    }
}

void afficheTemps(float tabTemps[6])
{
    printf("Tri insertion : %fs\nTri à bulles : %fs\n", tabTemps[0], tabTemps[1]);
    printf("Tri selection : %fs\nTri rapide : %fs\n", tabTemps[2], tabTemps[3]);
    printf("Tri fusion : %fs\nTri shell : %fs\n", tabTemps[4], tabTemps[5]);
}