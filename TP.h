/** @file TP.h
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 11:46
 *
 *  @brief 
 *
 */

#ifndef __TP_H__
#define __TP_H__

#include "lib/samIO.h"
#include "lib/samTabDyn.h"
#include "tris.h"
#include <time.h>

/** @fn TabInt1D randTab(int n, int Inf, int Sup)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 11:50
 *
 *  @brief Crée et renvoi un tableau d'entier aleatoir
 *
 *  @param tab : tableau a alloué et initialiser
 *  @param n : taille du tableau
 *  @param Inf : valeur minimum du tableau
 *  @param Sup : valeur maximum du tableau
 *
 */
TabInt1D randTab(int n, int Inf, int Sup);

/** @fn void copieTab (TabInt1D tab1, TabInt1D tab2)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 14:57
 *
 *  @brief Réalise la copie d'un tableau
 *
 *  @param tab1 : pointeur vers le premier tableau
 *  @param tab2 : pointeur vers la copie du tableau
 *
 */
void copieTab(TabInt1D *tab1, TabInt1D *tab2);

/** @fn void executeTri (int numTri, TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:57
 *
 *  @brief execute le bon tri en fonction de l'indice entré
 *
 *  @param numTri : numeros du tri a executer
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void executeTri(int numTri, TabInt1D *tab);

/** @fn void execTestTri(int taille,float tabTemps[6])
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 16:04
 *
 *  @brief test chaque tris et stock leur temps d'execution dans un tableau
 *
 *  @param taille : taille du tableau a tester
 *  @return retourne un tableau contenant le temps d'execution de chaque tri
 *
 */
void execTestTri(int taille, float tabTemps[6]);

/** @fn void afficheTemps (float tabTemps[6])
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 16:16
 *
 *  @brief Affiche le temps pris par chaque algo pour trié les tableau
 *
 *  @param tabTemp: tableau contenant le temps d'execution de chaque tri
 *
 */
void afficheTemps(float tabTemps[6]);

#endif // __TP_H__
