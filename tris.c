/** @file tris.c
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:06
 *
 *  @brief 
 *
 */

#include "tris.h"

void triInsertion(TabInt1D *tab)
{
    int int_i;
    int tmp;
    int int_j;

    for (int_i = 1; int_i < tab->nbCases; int_i++)
    {
        tmp = tab->tableau[int_i];
        int_j = int_i - 1;
        while (int_j >= 0 && tab->tableau[int_j] > tmp)
        {
            tab->tableau[int_j + 1] = tab->tableau[int_j];
            int_j = int_j - 1;
        }
        tab->tableau[int_j + 1] = tmp;
    }
}

void echange(int *val1, int *val2)
{
    int tmp = *val1;
    *val1 = *val2;
    *val2 = tmp;
}

void triBulles(TabInt1D *tab)
{
    int int_i;
    int int_j;
    for (int_i = 0; int_i < tab->nbCases - 1; int_i++)
        for (int_j = 0; int_j < tab->nbCases - int_i - 1; int_j++)
            if (tab->tableau[int_j] > tab->tableau[int_j + 1])
                echange(&tab->tableau[int_j], &tab->tableau[int_j + 1]);
}

void triSelection(TabInt1D *tab)
{
    int int_i;
    int int_j;
    int indice_min;

    for (int_i = 0; int_i < tab->nbCases - 1; int_i++)
    {
        indice_min = int_i;
        for (int_j = int_i + 1; int_j < tab->nbCases; int_j++)
            if (tab->tableau[int_j] < tab->tableau[indice_min])
                indice_min = int_j;
        echange(&tab->tableau[indice_min], &tab->tableau[int_i]);
    }
}

int partition(int *tab, int debut, int fin)
{
    int pivot = tab[fin];
    int int_i = (debut - 1);

    for (int j = debut; j <= fin - 1; j++)
    {
        if (tab[j] < pivot)
        {
            int_i++;
            echange(&tab[int_i], &tab[j]);
        }
    }
    echange(&tab[int_i + 1], &tab[fin]);
    return (int_i + 1);
}

void foncTriRapide(int *tab, int debut, int fin)
{
    if (debut < fin)
    {
        int pivot = partition(tab, debut, fin);

        foncTriRapide(tab, debut, pivot - 1);
        foncTriRapide(tab, pivot + 1, fin);
    }
}

void triRapide(TabInt1D *tab)
{
    foncTriRapide(tab->tableau, 0, tab->nbCases - 1);
}

void fusion(int *tab, int debut, int millieu, int fin)
{
    int int_i, int_j, k;
    int n1 = millieu - debut + 1;
    int n2 = fin - millieu;

    /* crée des tableau temporaire */
    int *tabGauche = allocTabInt1D(n1);
    int *tabDroite = allocTabInt1D(n2);
    /* divise tab en deux dans tabGauche et tabDroite */
    for (int_i = 0; int_i < n1; int_i++)
        tabGauche[int_i] = tab[debut + int_i];
    for (int_j = 0; int_j < n2; int_j++)
        tabDroite[int_j] = tab[millieu + 1 + int_j];

    /* fusion les deux tableaux*/
    int_i = 0; // Indice initiale du premier sous tableau
    int_j = 0; // Indice initiale du deuxieme sous tableau
    k = debut; // Indice initiale du tableau fusioné
    while (int_i < n1 && int_j < n2)
    {
        if (tabGauche[int_i] <= tabDroite[int_j])
        {
            tab[k] = tabGauche[int_i];
            int_i++;
        }
        else
        {
            tab[k] = tabDroite[int_j];
            int_j++;
        }
        k++;
    }

    while (int_i < n1)
    {
        tab[k] = tabGauche[int_i];
        int_i++;
        k++;
    }

    while (int_j < n2)
    {
        tab[k] = tabDroite[int_j];
        int_j++;
        k++;
    }
    free(tabGauche);
    free(tabDroite);
}

void foncTriFusion(int *tab, int debut, int fin)
{
    if (debut < fin)
    {
        int m = debut + (fin - debut) / 2;

        foncTriFusion(tab, debut, m);
        foncTriFusion(tab, m + 1, fin);

        fusion(tab, debut, m, fin);
    }
}

void triFusion(TabInt1D *tab)
{
    foncTriFusion(tab->tableau, 0, tab->nbCases - 1);
}

void triShell(TabInt1D *tab)
{
    int ecart;
    int int_i;
    int int_j;
    int tmp;

    for (ecart = tab->nbCases / 2; ecart > 0; ecart /= 2)
    {
        for (int_i = ecart; int_i < tab->nbCases; int_i += 1)
        {
            tmp = tab->tableau[int_i];
            for (int_j = int_i; int_j >= ecart && tab->tableau[int_j - ecart] > tmp; int_j -= ecart)
                tab->tableau[int_j] = tab->tableau[int_j - ecart];

            tab->tableau[int_j] = tmp;
        }
    }
}