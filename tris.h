/** @file tris.h
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:06
 *
 *  @brief 
 *
 */

#ifndef __TRIS_H__
#define __TRIS_H__

#include "lib/samTabDyn.h"

/** @fn void triInsertion (TabInt1D tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:07
 *
 *  @brief Tri un tableau grace au tri par insertion
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triInsertion(TabInt1D *tab);

/** @fn void echange (int *val1, int *val2)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:11
 *
 *  @brief Echange deux entier
 *
 *  @param val1 : premier valeur a echanger
 *  @param val2 : deuxieme valeur a echanger
 * 
 */
void echange(int *val1, int *val2);

/** @fn void triBulles (TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:13
 *
 *  @brief Tri un tableau grace au tri a bulles
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triBulles(TabInt1D *tab);

/** @fn void triSelection (TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:17
 *
 *  @brief Tri un tableau grace au tri par selection
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triSelection(TabInt1D *tab);

/** @fn int partition (int *tab, int debut, int fin)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:20
 *
 *  @brief Fonction de partitionement du tris rapide
 *
 *  @param tab : pointeur vers le premier element du tableau
 *  @param debut : indice du debut de la partition
 *  @param fin : indice de la fin de la partition
 *  @return la valeur du pivot
 *
 */
int partition(int *tab, int debut, int fin);

/** @fn void foncTriRapide (int *tab, int debut, int fin)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:25
 *
 *  @brief Tri un tableau grace au tri rapide
 *
 *  @param tab : pointeur vers le premier element du tableau
 *  @param debut : indice du debut de la partition
 *  @param fin : indice de la fin de la partition
 *
 */
void foncTriRapide(int *tab, int debut, int fin);

/** @fn void triRapide (TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:29
 *
 *  @brief Execute le tri rapide
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triRapide(TabInt1D *tab);

/** @fn void fusion (int *tab, int debut,int millieu, int fin)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:33
 *
 *  @brief Fonction de fucion du tri Fusion
 *
 *  @param tab : pointeur vers le premier element du tableau
 *  @param debut : debut de l'emplcement ou fusionné les tableau
 *  @param millieu du sous tableau a fusionné
 *  @param fin : fin de l'emplcement ou fusionné les tableau
 *
 */
void fusion(int *tab, int debut, int millieu, int fin);

/** @fn void foncTriFusion (int *tab, int debut, int fin)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:44
 *
 *  @brief Tri un tableau grace au tri fusion
 *
 *  @param tab : pointeur vers le premier element du tableau
 *  @param debut : indice du debut de la partition
 *  @param fin : indice de la fin de la partition
 *
 */
void foncTriFusion(int *tab, int debut, int fin);

/** @fn void triFusion (TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:47
 *
 *  @brief Execute le tri fusion
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triFusion(TabInt1D *tab);

/** @fn void triShell (TabInt1D *tab)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 15:51
 *
 *  @brief Tri un tableau grace au tri shell
 *
 *  @param tab : pointeur vers le tableau a trier
 *
 */
void triShell(TabInt1D *tab);

#endif // __TRIS_H__
