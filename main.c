/** @file main.c
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 17 Mar 2020 11:46
 *
 *  @brief 
 *
 */

#include "main.h"

int main(int argc, char const *argv[])
{
    int taille;
    float tabTemps[6];
    printf("Quel taille de tableau ? :");
    saisieInt(&taille);
    system("clear");

    srand(time(NULL));
    execTestTri(taille, tabTemps);
    afficheTemps(tabTemps);
    return 0;
}
