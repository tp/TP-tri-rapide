/** @file samTabDyn.h
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:23
 *
 *  @brief 
 *
 */

#ifndef __SAMTABDYN_H__
#define __SAMTABDYN_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

//-----------Tab Entier-----------//

/**
 * @struct TabInt1D
 *
 * @brief Structure pour stocker un tableau d'entier à une dimension
 *
 */
typedef struct
{
    /*@{*/
    int *tableau;
    int nbCases;
    /*@}*/
} TabInt1D;

/**
 * @struct TabInt2D
 *
 * @brief Structure pour stocker un tableau d'entier à une dimension
 *
 */
typedef struct
{
    /*@{*/
    int **tableau;
    int nbLignes;
    int nbColonnes;
    /*@}*/
} TabInt2D;

/** @fn int* allocTabInt1D (int nbCases)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 21:57
 *
 *  @brief Alloue l'espace memoire pour le tableau dynamique
 *
 *  @param nbCases : la taille du tableau a créer
 *  @return	tab : tableau créé
 *
 *  @remarks
 */
int *allocTabInt1D(int nbCases);

/** @fn void iniTabInt1D(TabInt1D *tab, int nbCases, int valInit)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 21:49
 *
 *  @brief Crée un tableau dynamique et le remplie de 0
 *
 *  @param tab : pointeur vers la structure du tableau a créer
 *  @param nbCases : la taille du tableau a créer
 *  @param valInit : valeur a laquel le tableau doit être initialisé
 *  @return tab : structure contenant le tableau créé
 *
 *  @remarks
 */
void iniTabInt1D(TabInt1D *tab, int nbCases, int valInit);

/** @fn void afficheTabInt1D (TabInt1D tabInt1D)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 22:22
 *
 *  @brief Affiche le tableau de frequence
 *
 *  @param tabFreq1D : Struct du tableau à afficher
 *
 */
void afficheTabInt1D(TabInt1D tabInt1D);

/** @fn int** allocTabInt2D (int nbLignes, int nbColonnes)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:33
 *
 *  @brief Allou l'espace memoire pour un tableau d'entier a deux dimensions
 *
 *  @param nbLignes : nombres de lignes dans le tableau
 *  @param nbColonnes : nombre de colonnes dans le tableau 
 *  @return tab : l'adresse du tableau allouer
 *
 */
int **allocTabInt2D(int nbLignes, int nbColonnes);

/** @fn void iniTabInt2D(TabInt2D *tab, int nbLignes, int nbColonnes, int valInit)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:40
 *
 *  @brief Alloue et initialise un tableau a une valeur specifier
 *  
 *  @param tab : pointeur vers la structure du tableau a créer
 *  @param nbLignes : nombres de lignes dans le tableau
 *  @param nbColonnes : nombre de colonnes dans le tableau 
 *  @param valInit : valeur d'initialisation
 *  @return tab : la structure du tableau créé
 *
 */
void iniTabInt2D(TabInt2D *tab, int nbLignes, int nbColonnes, int valInit);

/** @fn void afficheTabInt2D (TabInt2D tabInt2D)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 28 Jan 2020 15:41
 *
 *  @brief Affiche un tableau d'entier a 2 dimension
 *
 *  @param tabInt2D : tableau a afficher
 *
 */
void afficheTabInt2D(TabInt2D tabInt2D);

//-----------Tab Double-----------//

/**
 * @struct TabDouble1D
 *
 * @brief Structure pour stocker un tableau de double à une dimension
 *
 */
typedef struct
{
    /*@{*/
    double *tableau;
    int nbCases;
    /*@}*/
} TabDouble1D;

/**
 * @struct TabDouble2D
 *
 * @brief Structure pour stocker un tableau d'entier à une dimension
 *
 */
typedef struct
{
    /*@{*/
    double **tableau;
    int nbLignes;
    int nbColonnes;
    /*@}*/
} TabDouble2D;

/** @fn double *allocTabDouble1D(double nbCases)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 21:57
 *
 *  @brief Alloue l'espace memoire pour le tableau dynamique
 *
 *  @param nbCases : la taille du tableau a créer
 *  @return	tab : tableau créé
 *
 *  @remarks
 */
double *allocTabDouble1D(double nbCases);

/** @fn void iniTabDouble1D(TabDouble1D *tab, int nbCases, double valInit)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 21:49
 *
 *  @brief Crée un tableau dynamique et le remplie de 0
 *
 *  @param tab : pointeur vers la structure du tableau a créer
 *  @param nbCases : la taille du tableau a créer
 *  @param valInit : valeur a laquel le tableau doit être initialisé
 *  @return tab : structure contenant le tableau créé
 *
 *  @remarks
 */
void iniTabDouble1D(TabDouble1D *tab, int nbCases, double valInit);

/** @fn void afficheTabDouble1D(TabDouble1D tabDouble1D)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 21 Jan 2020 22:22
 *
 *  @brief Affiche le tableau
 *
 *  @param tabDouble1D : Struct du tableau à afficher
 *
 */
void afficheTabDouble1D(TabDouble1D tabDouble1D);

/** @fn double **allocTabDouble2D(int nbLignes, int nbColonnes)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:33
 *
 *  @brief Allou l'espace memoire pour un tableau d'entier a deux dimensions
 *
 *  @param nbLignes : nombres de lignes dans le tableau
 *  @param nbColonnes : nombre de colonnes dans le tableau 
 *  @return tab : l'adresse du tableau allouer
 *
 */
double **allocTabDouble2D(int nbLignes, int nbColonnes);

/** @fn void iniTabDouble2D(TabDouble2D *tab, int nbLignes, int nbColonnes, double valInit)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:40
 *
 *  @brief Alloue et initialise un tableau a une valeur specifier
 *  
 *  @param tab : pointeur vers la structure du tableau a créer
 *  @param nbLignes : nombres de lignes dans le tableau
 *  @param nbColonnes : nombre de colonnes dans le tableau 
 *  @param valInit : valeur d'initialisation
 *  @return tab : la structure du tableau créé
 *
 */
void iniTabDouble2D(TabDouble2D *tab, int nbLignes, int nbColonnes, double valInit);

/** @fn void afficheTabInt2D(TabDouble2D tabDouble2D)
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Tue 28 Jan 2020 15:41
 *
 *  @brief Affiche un tableau d'entier a 2 dimension
 *
 *  @param tabDouble2D : tableau a afficher
 *
 */
void afficheTabDouble2D(TabDouble2D tabDouble2D);

//-----------Tab Chaine-----------//

/**
 * @struct TabChaine
 *
 * @brief Structure pour stocker un tableau de chaine de caractère
 *
 */
typedef struct
{
    /*@{*/
    char **tableau;
    int nbChaines;
    int tailleChaines;
    /*@}*/
} TabChaine;

#endif // __SAMTABDYN_H__
