/** @file samTabDyn.c
 *  @author Samuel Rodrigues <samuel.rodrigues@eisti.eu>
 *  @version 0.1
 *  @date Mon 27 Jan 2020 15:29
 *
 *  @brief 
 *
 */
#include "samTabDyn.h"

//-----------Tab Entier-----------//

int *allocTabInt1D(int nbCases)
{
    int *tab; // Variable de retour

    tab = (int *)malloc(sizeof(int) * nbCases);

    if (tab == NULL)
    { //Test si l'allocation a bien eu lieu
        fprintf(stderr, "Pb d'allocation Tab 1D\n");
        exit(1);
    }

    return (tab);
}

void iniTabInt1D(TabInt1D *tab, int nbCases, int valInit)
{
    tab->nbCases = nbCases;

    tab->tableau = allocTabInt1D(nbCases);

    int int_case;
    for (int_case = 0; int_case < nbCases; int_case++)
    {
        tab->tableau[int_case] = valInit;
    }
}

void afficheTabInt1D(TabInt1D tabInt1D)
{
    int int_case;
    for (int_case = 0; int_case < tabInt1D.nbCases; int_case++)
    {
        printf("|%d", tabInt1D.tableau[int_case]);
    }
    printf("|\n");
}

int **allocTabInt2D(int nbLignes, int nbColonnes)
{
    int **tab; // Variable de retour
    tab = malloc(sizeof(int *) * nbLignes);
    if (tab == NULL)
    {
        fprintf(stderr, "Pb d'allocation Tab 2D\n");
        exit(1);
    }
    int int_Ligne;
    for (int_Ligne = 0; int_Ligne < nbLignes; int_Ligne++)
    {
        tab[int_Ligne] = malloc(sizeof(int) * nbColonnes);
        if (tab[int_Ligne] == NULL)
        {
            fprintf(stderr, "Pb d'allocation Tab 2D\n");
            exit(1);
        }
    }

    return (tab);
}

void iniTabInt2D(TabInt2D *tab, int nbLignes, int nbColonnes, int valInit)
{
    int int_ligne;
    int int_colonne;

    tab->tableau = allocTabInt2D(nbLignes, nbColonnes);
    tab->nbLignes = nbLignes;
    tab->nbColonnes = nbColonnes;

    for (int_ligne = 0; int_ligne < nbLignes; int_ligne++)
    {
        for (int_colonne = 0; int_colonne < nbColonnes; int_colonne++)
        {
            tab->tableau[int_ligne][int_colonne] = valInit;
        }
    }
}

void afficheTabInt2D(TabInt2D tabInt2D)
{
    int int_ligne;
    int int_colonne;
    for (int_ligne = 0; int_ligne < tabInt2D.nbLignes; int_ligne++)
    {
        for (int_colonne = 0; int_colonne < tabInt2D.nbColonnes; int_colonne++)
        {
            printf("| %d ", tabInt2D.tableau[int_ligne][int_colonne]);
        }
        printf("|");
    }
}

//-----------Tab Double-----------//

double *allocTabDouble1D(double nbCases)
{
    double *tab; // Variable de retour

    tab = (double *)malloc(sizeof(double) * nbCases);

    if (tab == NULL)
    { //Test si l'allocation a bien eu lieu
        fprintf(stderr, "Pb d'allocation Tab 1D\n");
        exit(1);
    }

    return (tab);
}

void iniTabDouble1D(TabDouble1D *tab, int nbCases, double valInit)
{
    tab->nbCases = nbCases;

    tab->tableau = allocTabDouble1D(nbCases);

    int int_case;
    for (int_case = 0; int_case < nbCases; int_case++)
    {
        tab->tableau[int_case] = valInit;
    }
}

void afficheTabDouble1D(TabDouble1D tabDouble1D)
{
    int int_case;
    for (int_case = 0; int_case < tabDouble1D.nbCases; int_case++)
    {
        printf("|%lf", tabDouble1D.tableau[int_case]);
    }
    printf("|\n");
}

double **allocTabDouble2D(int nbLignes, int nbColonnes)
{
    double **tab; // Variable de retour
    tab = malloc(sizeof(double *) * nbLignes);
    if (tab == NULL)
    {
        fprintf(stderr, "Pb d'allocation Tab 2D\n");
        exit(1);
    }
    int int_Ligne;
    for (int_Ligne = 0; int_Ligne < nbLignes; int_Ligne++)
    {
        tab[int_Ligne] = malloc(sizeof(double) * nbColonnes);
        if (tab[int_Ligne] == NULL)
        {
            fprintf(stderr, "Pb d'allocation Tab 2D\n");
            exit(1);
        }
    }

    return (tab);
}

void iniTabDouble2D(TabDouble2D *tab, int nbLignes, int nbColonnes, double valInit)
{
    int int_ligne;
    int int_colonne;

    tab->tableau = allocTabDouble2D(nbLignes, nbColonnes);
    tab->nbLignes = nbLignes;
    tab->nbColonnes = nbColonnes;

    for (int_ligne = 0; int_ligne < nbLignes; int_ligne++)
    {
        for (int_colonne = 0; int_colonne < nbColonnes; int_colonne++)
        {
            tab->tableau[int_ligne][int_colonne] = valInit;
        }
    }
}

void afficheTabDouble2D(TabDouble2D tabDouble2D)
{
    int int_ligne;
    int int_colonne;
    for (int_ligne = 0; int_ligne < tabDouble2D.nbLignes; int_ligne++)
    {
        for (int_colonne = 0; int_colonne < tabDouble2D.nbColonnes; int_colonne++)
        {
            printf("| %lf ", tabDouble2D.tableau[int_ligne][int_colonne]);
        }
        printf("|");
    }
}
